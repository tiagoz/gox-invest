var config = require('./config')
var log_level = (process.env.LOG_LEVEL) ? process.env.LOG_LEVEL : config.log_level
var bunyan = require('bunyan')
var log = bunyan.createLogger({
    name: "snep-cs",
    level: log_level,
    src: true
})

module.exports = log
