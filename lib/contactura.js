var db = require('mysql');
var snep = require('./snep');
var log = require('./log');
var moment = require('moment');
const Client = require('node-rest-client').Client;

var DBContact = db.createPool({
  connectionLimit : 100,
  host: (process.env.MYSQL_HOST || 'mariadb.opens.com.br'),
  port: (process.env.MYSQL_PORT || '3306'),
  user: (process.env.MYSQL_USER || 'ctura_webapi'),
  password: (process.env.MYSQL_PASSWORD || 'G$#XAl0=UN256STp,NCP'),
  database: (process.env.MYSQL_DATABASE || 'ctura_webapi')
});

// update name client on automation_client
function updateNameClient(cod_client, name, callback){
  
  var query = "update automation_client set name = '"+name+"' where cod_client = "+ cod_client;
  DBContact.query(query, function(error, rows) {
    if (error) return callback(error);

    return callback(null, rows);
  })
}

// update scopes and token expires date
function updateScopeAndValidate(token, scope, validate, callback){
  
  var query = "update oauth_access_tokens set scope = '"+scope+"', expires = '"+validate+"' where access_token = '"+ token+ "'";
  DBContact.query(query, function(error, rows) {
    if (error) return callback(error);

    return callback(null, rows);
  })
}

function getClientAndToken(cod_client, token, callback){

  var select = "Select c.cod_client, c.name, c.email, c.created, t.access_token, t.expires, t.user_id, t.scope from automation_client as c inner join oauth_access_tokens as t on c.cod_client = t.client_id where c.cod_client = "+ cod_client + " AND t.access_token = '" + token + "'";
  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    
    return callback(rows);
  })
}

function getClients(callback) {

  var select = "Select c.cod_client, c.name, c.email, c.created, t.access_token, t.client_id, t.expires, t.scope from automation_client as c inner join oauth_access_tokens as t on c.cod_client = t.client_id order by c.cod_client";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    return callback(rows);
  })
}

function getKiperData(callback) {

  var select = "Select validation_tokens.id, validation_tokens.date,oauth_access_tokens.user_id,oauth_access_tokens.client_id, validation_tokens.access_token,validation_tokens.device_uuid,validation_tokens.value,validation_tokens.group from validation_tokens inner join oauth_access_tokens on validation_tokens.access_token = oauth_access_tokens.access_token where service='kiper'";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    return callback(rows);
  })
}

function getQmanagerData(callback) {

  var select = "Select validation_tokens.id, validation_tokens.date,oauth_access_tokens.user_id,oauth_access_tokens.client_id, validation_tokens.access_token,validation_tokens.device_uuid,validation_tokens.value,validation_tokens.group from validation_tokens inner join oauth_access_tokens on validation_tokens.access_token = oauth_access_tokens.access_token where service='q-manager'";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    return callback(rows);
  })
}

function getSituatorData(callback) {

  var select = "Select validation_tokens.date,oauth_access_tokens.user_id,oauth_access_tokens.client_id, validation_tokens.access_token,validation_tokens.device_uuid,validation_tokens.value,validation_tokens.group from validation_tokens inner join oauth_access_tokens on validation_tokens.access_token = oauth_access_tokens.access_token where service='situator'";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    return callback(rows);
  })
}

function removeValidation(id, callback){

  var query = DBContact.query("Delete from validation_tokens where id= '"+id+"'", function(err, result) {
    if (err) return callback({ status: 'error', message: err });
    return callback(null, result);
  });

}

function getListSituator(callback) {

  var today = moment().format('DD.MM.YYYY');
  today = moment(today, "DD.MM.YYYY");

  graphic = { "01": 0, "02": 0, "03": 0, "04": 0, "05": 0, "06": 0, "07": 0, "08": 0, "09": 0, "10": 0, "11": 0, "12": 0 };

  getClients(function(clients) {    
    getSituatorData(function(data) {

      Object.keys(data).map(function(key) {
        if (data[key].client_id == 1236 || data[key].client_id == 1199) { // 1236 showroom seventh 1199 Carlos Teste
          delete data[key];
        } else {
          Object.keys(clients).map(function(cli) {

            switch (data[key].group) {
              case '1':
                data[key].class = 'label label-success';
                data[key].title = 'Grupo 1 - Até 4 Ramais';
                data[key].valuePrice = 100 * parseInt(data[key].value);
                break;
              case '2':
                data[key].class = 'label label-warning';
                data[key].title = 'Grupo 2 - Até 5 Ramais';
                data[key].valuePrice = 120 * parseInt(data[key].value);
                break;
              case '3':
                data[key].class = 'label label-danger';
                data[key].title = 'Grupo 3 - Mais de 6 Ramais';
                data[key].valuePrice = 140 * (data[key].value);
                break;
              default:
                data[key].class = 'label label-default';
                break;
            }

            if (data[key].client_id == clients[cli].cod_client) {
              data[key].name = clients[cli].name;
              data[key].created = clients[cli].created;

              var created = moment(clients[cli].created).format("DD.MM.YYYY");
              created = moment(clients[cli].created, "DD.MM.YYYY");
              var diff = today.diff(created, 'days');

              if (diff <= 30) {
                data[key].free = true;
              }

              var updated = moment(data[key].date).format("DD.MM.YYYY");
              updated = moment(data[key].date, "DD.MM.YYYY");
              var diffUpdated = today.diff(updated, 'days');

              if (diffUpdated <= 5) {
                data[key].classupdate = "label label-success";
              } else if (diffUpdated > 5 && diffUpdated <= 15) {
                data[key].classupdate = "label label-warning";
              } else {
                data[key].classupdate = "label label-danger";
              }

              data[key].expires = clients[cli].expires;
              var expires = moment(data[key].expires).format("DD.MM.YYYY");
              expires = moment(data[key].expires, "DD.MM.YYYY");
              var diffExpires = today.diff(expires, "days");
              data[key].classExpires = "";
              data[key].titleExpires = "";

              if (diffExpires > -15 && diffExpires < 0) {
                data[key].classExpires = "label label-danger";
                data[key].titleExpires = "Faltam apenas " + diffExpires + " dias para expirar o token do cliente. Caso seja cliente contratado deve-se renovar esta data.";
              } else if (diffExpires >= 0) {
                data[key].classExpires = "label label-default";
                data[key].titleExpires = "Token expirado a " + diffExpires + " dias.";
              }

            }

          });
        }
      });

      var bill = {
        group1: 0,
        group2: 0,
        group3: 0,
        install: 0,
        totalGroup: 0,
        group1Value: 0,
        group2Value: 0,
        group3Value: 0,
        installValue: 0,
        group1Price: 100,
        group2Price: 120,
        group3Price: 140,
        group1Perc: 0,
        group2Perc: 0,
        group3Perc: 0,
        installPrice: 500,
        totalValueOnlyCond: 0,
        totalValue: 0,
      }

      Object.keys(data).map(function(key) {

        if (data[key].value > 0) {

          if (typeof data[key].valueTotal != 'undefined') {
            data[key].valueTotal += parseInt(data[key].value);
          } else {
            data[key].valueTotal = parseInt(data[key].value);
          }

          var month = moment(data[key].created).format('MM-YYYY');

          var yearGraphic = moment(data[key].created).format('YYYY');
          var monthGraphic = moment(data[key].created).format('MM');
          if (yearGraphic == '2017') {
            graphic[monthGraphic]++;
          }

          var created = moment(data[key].created).format("DD.MM.YYYY");
          created = moment(data[key].created, "DD.MM.YYYY");
          var diff = today.diff(created, 'days');

          if (diff <= 30) {

            bill.install++;
            bill.installValue = bill.install * bill.installPrice;
          } else {
            switch (data[key].group) {
              case '1':
                bill.group1 = parseInt(bill.group1) + parseInt(data[key].value);
                bill.group1Value = bill.group1 * bill.group1Price;

                if (data[key].valueTotal > 1) {
                  data[key].discardShowRoom = true;
                  bill.group1 = parseInt(bill.group1) - 1;
                  //bill.group1Value = bill.group1 * bill.group1Price;
                  data[key].value = data[key].value - 1;
                  data[key].valuePrice = 100 * parseInt(data[key].value);
                }
                bill.totalValue += bill.group1Value;
                break;
              case '2':
                bill.group2 = parseInt(bill.group2) + parseInt(data[key].value);
                bill.group2Value = bill.group2 * bill.group2Price;
                bill.totalValue += bill.group2Value;
                break;
              case '3':
                bill.group3 = parseInt(bill.group3) + parseInt(data[key].value);
                bill.group3Value = bill.group3 * bill.group3Price;
                bill.totalValue += bill.group3Value;
                break;
            }
            bill.totalGroup = bill.group1 + bill.group2 + bill.group3;
          }
          bill.totalValueOnlyCond = bill.group1Value + bill.group2Value + bill.group3Value;
          bill.totalValue = bill.group1Value + bill.group2Value + bill.group3Value + bill.installValue;

          bill.group1Perc = Math.round((bill.group1 * 100) / bill.totalGroup)
          bill.group2Perc = Math.round((bill.group2 * 100) / bill.totalGroup)
          bill.group3Perc = Math.round((bill.group3 * 100) / bill.totalGroup)
        }
      })

      return callback(data, bill, graphic);
    })
  });

}

function getListKiper(callback) {

  var today = moment().format('DD.MM.YYYY');
  today = moment(today, "DD.MM.YYYY");

  getClients(function(clients) {
    getKiperData(function(data) {

      Object.keys(data).map(function(key) {
        Object.keys(clients).map(function(cli) {
          if (data[key].client_id == clients[cli].cod_client) {
            data[key].name = clients[cli].name;
            data[key].expires = clients[cli].expires;

            if(data[key].device_uuid.indexOf(';') > 0){
              var v = data[key].device_uuid.split(';');
              data[key].device_uuid = v[0];
              data[key].version = v[2];
              data[key].name2 = v[1];
            }
          }
        });
      });


      var result = { total: 0 };
      var info = [];
      Object.keys(data).map(function(key) {
        if (data[key].value > 0 && data[key].client_id != 1199 && data[key].device_uuid != "c879b4e8-8d3c-488e-a3ce-065b08d6e447") {
          // code teste Carlos

          var updated = moment(data[key].date).format("DD.MM.YYYY");
          updated = moment(data[key].date, "DD.MM.YYYY");
          var diffUpdated = today.diff(updated, "days");

          if (diffUpdated <= 5) {
            data[key].class = "label label-success";
          } else if (diffUpdated > 5 && diffUpdated <= 15) {
            data[key].class = "label label-warning";
          } else {
            data[key].class = "label label-danger";
          }

          info.push(data[key]);
          result.total += data[key].value;
        }
      })

      result.info = info;
      return callback(result);
    })
  })
}

function getListQmanager(callback) {

  var today = moment().format('DD.MM.YYYY');
  today = moment(today, "DD.MM.YYYY");
  var result = { total: 0, trial: 0, totalSuccess: 0, totalFree: 0, totalTrial: 0 };

  getClients(function(clients) { 
    getQmanagerData(function(data) {

      Object.keys(data).map(function(key) {
        Object.keys(clients).map(function(cli) {
          if (clients[cli] && data[key].client_id == clients[cli].cod_client) {
            data[key].name = clients[cli].name;
            data[key].created = clients[cli].created;
            data[key].email = clients[cli].email;
            data[key].expires = clients[cli].expires;
            data[key].client_id = clients[cli].client_id;

            var created = moment(clients[cli].created).format("DD.MM.YYYY");
            created = moment(clients[cli].created, "DD.MM.YYYY");
            var diff = today.diff(created, 'days');

            if (diff <= 15 && data[key].value > 0) {
              data[key].trial = true;
              result.trial++;
            }

            var updated = moment(data[key].date).format("DD.MM.YYYY");
            updated = moment(data[key].date, "DD.MM.YYYY");
            var diffUpdated = today.diff(updated, 'days');

            if (diffUpdated <= 5) {
              data[key].class = "label label-success";
            } else if (diffUpdated > 5 && diffUpdated <= 15) {
              data[key].class = "label label-warning";
            } else {
              data[key].class = "label label-danger";
            }

            var expires = moment(data[key].expires).format("DD.MM.YYYY");
            expires = moment(data[key].expires, "DD.MM.YYYY");
            var diffExpires = today.diff(expires, "days");
            data[key].classExpires = "";
            data[key].titleExpires = "";

            if (diffExpires > -15 && diffExpires < 0) {
              data[key].classExpires = "label label-danger";
              data[key].titleExpires = "Faltam apenas " + diffExpires + " dias para expirar o token do cliente. Caso seja cliente contratado deve-se renovar esta data.";
            } else if (diffExpires >= 0) {
              data[key].classExpires = "label label-default";
              data[key].titleExpires = "Token expirado a " + diffExpires + " dias.";
            }

          }
        });
      });

      var info = [];
      Object.keys(data).map(function(key) {
        if (data[key].value >= 0) {
          info.push(data[key]);
          result.total += data[key].value;
        }
      })

      result.info = info;
      return callback(result);
    })
  })
}

function getValidationTokens(callback) {

  var select = "Select * from validation_tokens";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);


    return callback(rows);
  })
}

function getListClientsAndTokens(callback) {

  var select = "Select c.cod_client, c.name, c.email, t.access_token, t.client_id, t.expires, t.scope from automation_client as c inner join oauth_access_tokens as t on c.cod_client = t.client_id order by c.cod_client";

  DBContact.query(select, function(error, rows) {
    if (error) return callback(error);

    var today = moment().format('DD.MM.YYYY');
    today = moment(today, "DD.MM.YYYY");

    getValidationTokens(function(clients) {

      Object.keys(rows).map(function(key) {
        Object.keys(clients).map(function(cli) {

          var expires = moment(rows[key].expires, "DD.MM.YYYY");
          var diff = expires.diff(today, 'days');

          if (diff < 0) {
            rows[key].classExpires = 'label label-default';
          } else if (diff > 0 && diff <= 30) {
            rows[key].classExpires = 'label label-danger';
          } else if (diff > 30 && diff <= 60) {
            rows[key].classExpires = 'label label-warning';
          } else {
            rows[key].classExpires = 'label label-success';
          }

          if (rows[key].access_token == clients[cli].access_token) {
            rows[key].active = true;
            rows[key].activeDate = clients[cli].date;
          }

        })
      })
      return callback(rows);
    })


  });
}

module.exports = { getSituatorData, getClientAndToken, updateNameClient, updateScopeAndValidate, getClients, getListSituator, getListKiper, getListQmanager, getQmanagerData, getListClientsAndTokens, removeValidation }