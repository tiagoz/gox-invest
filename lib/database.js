var mongo_host = process.env.MONGO_PORT_27017_TCP_ADDR ? process.env.MONGO_PORT_27017_TCP_ADDR : '127.0.0.1';
var mongo_port = process.env.MONGO_PORT_27017_TCP_PORT ? process.env.MONGO_PORT_27017_TCP_PORT : '27017';
var mongo_db = 'snep-cs';
var mongo_collection = 'snep';
var url = 'mongodb://' + mongo_host + ':' + mongo_port + '/';

module.exports = { 'url': url + mongo_db };

const mongoose = require('mongoose')

mongoose.Error.messages.general.required = "Key '{PATH}' is required!"
mongoose.Error.messages.Number.min = "The value '{VALUE}' < '{MIN}'."
mongoose.Error.messages.Number.max = "The '{VALUE}' > '{MAX}'."
mongoose.Error.messages.String.enum = "'{VALUE}' is not valid for '{PATH}'."