var request = require('request');
var config = require('./config');
var log = require('./log');
var request = require('request');
var querystring = require("querystring");

function getLocation(data, callback) {
  if (data && data.ip) {
    return request.get(config.localization.ip_api + '/' + data.ip, function(error, result, body) {
      if (error) return callback(error);

      return callback(null, body);
    })
  }
}

function register(req, res, next) {
  if (req.ip) {
    var forwarded = req.headers['x-forwarded-for'];
    var remoteAddress = req.connection.remoteAddress;
    log.debug('Searching geo-localization for: %s', req.ip);
    log.debug('Searching geo-localization for IPs: %s', req.ips);
    log.debug('Headers X-Forwarded-for: %s', forwarded);
    log.debug('Headers RemoteAddress: %s', remoteAddress);
    log.debug('Client IP: %s', req.clientIp);
    getLocation({ ip: req.ip.substr(7) }, function(error, result) {
      if (result) {
        log.debug('Localization: %s', JSON.stringify(result))
      } else {
        log.error('Error getting locatization: %s', JSON.stringify(error))
      }
      return next();
    })
  } else {
    log.debug('No IP found!');
    return next();
  }
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  if (!req.session.username) {
    return res.redirect('/auth/login');
  }
  next();
}

function getPermission(req, res, next) {

  users_.getUserById(req.session.userId, function(error, user) {
    if (error) return res.json(error)

    no_permission = true;

    const sess = req.session;
    sess.permissions = user[0].permissions;

    var views_permission = user[0].permissions;
    var model = req.originalUrl;
    model = model.split("/"); // Exampe:["","users","permission"]
    controller = model[1];
    controller = controller.replace("-", "_");
    action = model[2];

    // only controller
    if (typeof action == 'undefined') {
      if (views_permission[controller + "_view"] == true) {
        no_permission = false;
      }
    } else {
      if (views_permission[controller + "_action"] == true) {
        no_permission = false;
      }
    }

    if (no_permission) {
      console.log('No permission ' + no_permission + ' on controller: ' + controller);
      res.redirect('/');
    }
    next();
  });

}

function setProfile(req, res, next) {
  req.profile = {
    localization: {
      ip: req.ip
    }
  }
  return next();
}

function auth_device(credentials, callback) {
  var data = querystring.stringify({
    client_key: credentials.client_key,
    api_key: credentials.api_key,
    device_uuid: credentials.uuid
  });
  var url_data = '/scostumers?client_key=' + credentials.client_key + '&api_key=' + credentials.api_key + '&device_uuid=' + credentials.uuid;
  log.debug('Authenticating on ITC: %s', config.itc.auth + url_data);
  request(config.itc.auth + '/scostumers?' + data, function(error, response, body) {
    if (response && response.statusCode == 200) {
      return callback(null, JSON.parse(body));
    } else {
      return callback();
    }
  })
}

module.exports = {
  getLocation,
  register,
  setProfile,
  isLoggedIn,
  getPermission,
  auth_device
};
