var express = require('express');
var router = express.Router();
var functions = require('../lib/functions');

/* GET home page. */
//router.get('/', functions.isLoggedIn, function(req, res, next) {
router.get('/', function(req, res, next) {
  return res.render('dashboard', { title: 'Gox Invest - Dashboard', session: req.session });
});

module.exports = router;