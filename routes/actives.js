var express = require('express');
var router = express.Router();
var functions = require('../lib/functions');

router.get('/', function(req, res, next) {
  return res.render('actives/index', { title: 'Gox Invest - Ativos', session: req.session });
});

router.get('/add', function(req, res, next) {
  return res.render('actives/add', { title: 'Gox Invest - Ativos', session: req.session });
});

module.exports = router;